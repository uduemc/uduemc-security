package com.uduemc.security.browser.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.uduemc.security.core.SecurityProperties;

@Controller
public class BrowserController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SecurityProperties securityProperties;

	/**
	 * 请求缓存 HttpSessionRequestCache 默认的请求缓存接口实现
	 */
	// private RequestCache requestCache = new HttpSessionRequestCache();
	// 保存的 request
	// SavedRequest savedRequest = requestCache.getRequest(request, response);
	// 获取重定向的 url
	// String redirectUrl = savedRequest.getRedirectUrl();

	@GetMapping("/authentication/require")
	public Object signId(HttpServletRequest request, HttpServletResponse response) {
		String loginPage = securityProperties.getBrowser().getLoginPage();
		logger.info("securityProperties: "+loginPage);
		return loginPage;
	}
	
	/**
	 * 请求缓存 HttpSessionRequestCache 默认的请求缓存接口实现
	 */

	@GetMapping("/authentication/test")
	public String test() {
		return "authentication-test";
	}

}
