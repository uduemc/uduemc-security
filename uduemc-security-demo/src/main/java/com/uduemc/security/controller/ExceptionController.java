package com.uduemc.security.controller;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.uduemc.security.exception.NotFoundException;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(value = { NotFoundException.class })
	@ResponseBody
	// 返回错误状态码 500
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Object handleNotFindException(NotFoundException nfe) {
		System.out.println(nfe);

		Map<String, Object> result = new HashedMap<>();
		result.put("id", nfe.getId());
		result.put("message", nfe.getMessage());
		return result;
	}

}
