package com.uduemc.security.controller.aspect;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;

//@Aspect
//@Component
public class TimeAspect {

	/**
	 * 一般的情况下使用 @Around 注解
	 * 注解中的参数意义
	 * execution 执行
	 * * 任何返回类型
	 * com.uduemc.security.controller.CustomerController 类名
	 * CustomerController.* 类的所有方法
	 * (..) 方法的所有参数
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	@Around(value = "execution(* com.uduemc.security.controller.CustomerController.*(..))")
	public Object handleControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("   aspect start");
		// 打印参数看下
		Object[] args = pjp.getArgs();
		for (Object object : args) {
			System.out.println("    arg is: " + object);
		}
		long time = new Date().getTime();
		Object object = pjp.proceed();
		System.out.println("    aspect time: " + (new Date().getTime() - time));
		System.out.println("   aspect end");
		return object;
	}
}
