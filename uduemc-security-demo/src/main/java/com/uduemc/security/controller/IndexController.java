package com.uduemc.security.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

	@GetMapping("/me1")
	public Object me1() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@GetMapping("/me2")
	public Object me2(Authentication authentication) {
		return authentication;
	}
	
	/**
	 * 推荐使用
	 * @param user
	 * @return
	 */
	@GetMapping("/me3")
	public Object me3(@AuthenticationPrincipal UserDetails user) {
		return user;
	}

	@GetMapping("/")
	public Map<String, String> index() {
		Map<String, String> map = new HashMap<>();
		map.put("a", "aa");
		return map;
	}

}
