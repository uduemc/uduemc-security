package com.uduemc.security.controller.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.uduemc.security.controller.filter.TimeFilter;
import com.uduemc.security.controller.interceptor.TimeInterceptor;


@Configuration 
public class WebConfig extends WebMvcConfigurerAdapter{

	@Autowired
	private TimeInterceptor timeInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		super.addInterceptors(registry);
		registry.addInterceptor(timeInterceptor);
	}

	@Bean
	public FilterRegistrationBean timeFilter() {
		FilterRegistrationBean filterRegistrationBean  = new FilterRegistrationBean();
		
		/**
		 * 注册 filter
		 */
		TimeFilter filter = new TimeFilter();
		filterRegistrationBean.setFilter(filter);
		
		/**
		 * 配置生效url路径
		 */
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/*");
		filterRegistrationBean.setUrlPatterns(urlPatterns);
		
		return filterRegistrationBean;
	}
	
}
