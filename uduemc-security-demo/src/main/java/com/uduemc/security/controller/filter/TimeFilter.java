package com.uduemc.security.controller.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;

/**
 * 可以直接使用@Component注解实现Filter
 * 也可以使用配置的方式实现 Filter （/uduemc-security-demo/src/main/java/com/uduemc/security/controller/filter/TimeFilter.java）
 * 这两种方法都可以实现Filter，
 * 但是后者能够将第三方的Filter注册进来；
 * 并且可以定义需要过滤的Url路径；
 */
@Component
public class TimeFilter implements Filter {

	/**
	 * 初始化是调用的方法
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("filter: init");
	}

	/**
	 * 执行时调用的方法
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		long time = new Date().getTime();
		chain.doFilter(request, response);
		System.out.println("filter: doFilter(); time: " + (new Date().getTime() - time));
	}

	/**
	 * 销毁是调用的方法
	 */
	@Override
	public void destroy() {
		System.out.println("filter: destroy");
	}

}
