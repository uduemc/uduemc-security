package com.uduemc.security.extities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonView;
import com.uduemc.security.validator.MyConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "customer")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Customer implements Serializable {

	public interface customerSimpleView {
	};

	public interface customerDetailView extends customerSimpleView {
	};

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@JsonView(customerSimpleView.class)
	@MyConstraint(message = "此项 ID 在数据库中不存在")
	private Integer id;

	@Column(name = "name")
	@JsonView(customerSimpleView.class)
	@NotBlank(message = "客户的名称不能为空！")
	private String name;

	@Column(name = "age")
	@JsonView(customerSimpleView.class)
	private Short age;

	@Column(name = "sex")
	@JsonView(customerSimpleView.class)
	private Short sex;

	@Column(name = "auth_key")
	@JsonView(customerDetailView.class)
	private String authKey;

	@Column(name = "birth_day")
	@JsonView(customerSimpleView.class)
	@Past(message = "生日必须是一个过去的日期！")
	private Date birthDay;

	@Column(name = "create_at", insertable = false, updatable = false)
	@JsonView(customerDetailView.class)
	private Date createAt;
}