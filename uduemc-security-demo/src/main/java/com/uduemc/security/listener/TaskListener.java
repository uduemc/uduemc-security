package com.uduemc.security.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import com.uduemc.security.queue.TaskQueue;

@Component
public class TaskListener implements ApplicationListener<ContextRefreshedEvent> {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TaskQueue taskQueue;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				DeferredResult<Integer> deferredResult = taskQueue.poll();
				if (deferredResult != null) {
					logger.info("  处理 DeferredResult 实例");
					deferredResult.setResult(1);
				}
			}
		}).start();
	}

}
