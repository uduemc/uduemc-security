package com.uduemc.security.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

@Component
public class TaskQueue {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private BlockingQueue<DeferredResult<Integer>> queue = new LinkedBlockingDeque<>();

	public void offer(DeferredResult<Integer> deferredResult) throws InterruptedException {
		logger.info(" queue offer task");
		queue.put(deferredResult);
	}

	public DeferredResult<Integer> poll() {

		DeferredResult<Integer> poll = queue.poll();
		if (null != poll) {
			logger.info(" queue poll task");
		}
		return poll;
	}

}
