package com.uduemc.security.bean;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.uduemc.security.core.component.DefaultValidateCodeImage;
import com.uduemc.security.core.component.ImageCodeAndBufferedImage;

@Component
public class MyCustomValidateCodeImage extends DefaultValidateCodeImage {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public ImageCodeAndBufferedImage createImageCode(HttpServletRequest request) {
		logger.info("自定义的图片验证码类，只是继承了 默认的方式 DefaultValidateCodeImage");
		return super.createImageCode(request);
	}

}
