package com.uduemc.security.bean;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.uduemc.security.core.filter.ValidateCodeFilter;

@Component("customValidateCodeFilter")
public class CustomValidateCodeFilter extends ValidateCodeFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		logger.info("自定义处理验证码过滤器!只需要定义过滤器的名字 @Component(\"customValidateCodeFilter\") :" + request.getRequestURI());
		super.doFilterInternal(request, response, filterChain);
	}
}
