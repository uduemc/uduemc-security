package com.uduemc.security.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

// 需要继承 ConstraintValidator 接口 <A, T> A：注解接口 T：放在处理上类型
public class MyConstraintValidator implements ConstraintValidator<MyConstraint, Object> {

	@Override
	public void initialize(MyConstraint constraintAnnotation) {
		// 初始化
		System.out.println("MyConstraintValidator initialize");
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		// 具体操作逻辑
		System.out.println("isValid " + value);
		return false;
	}

}
