package com.uduemc.security.test.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestController {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void hello() throws Exception {
		// 模拟 get 请求
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		// 获取返回的结果
		String contentAsString = andReturn.getResponse().getContentAsString();
		assertEquals("{\"a\":\"aa\"}", contentAsString);
	}

	@Test
	public void customerControllerIndex() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/customer").param("username", "Jeff").param("age", "18")
				.param("size", "12").param("page", "1").param("sort", "age,asc")
				// 与上面不同的是增加了请求的类型
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 查看返回的数据长度是否是3个：更多资料可以在 github 中搜索 jsonpath 获取文档!
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3)).andDo(MockMvcResultHandlers.print())
				.andReturn();
	}

	/**
	 * 获取详细的客户数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void customerControllerCustomer() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/customer/1").contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1)).andDo(MockMvcResultHandlers.print())
				.andReturn();
	}

	/**
	 * 增加
	 * 
	 * @throws Exception
	 */
	@Test
	public void customerControllerCreate() throws Exception {
		String content = "{\"name\":\"Jeff\",\"age\":18,\"sex\":1}";
		// String content = "{\"name\":\"\",\"age\":18,\"sex\":1}";
		mockMvc.perform(
				MockMvcRequestBuilders.post("/create").content(content).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1)).andDo(MockMvcResultHandlers.print())
				.andReturn();
	}

	@Test
	public void customerControllerUpdate() throws Exception {
		// 一年以后的时间
		Date out = new Date(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		String content = "{\"id\":1,\"name\":\"Jeff\",\"age\":28,\"birthDay\": " + out.getTime() + "}";
		// String content = "{\"name\":\"\",\"age\":18,\"sex\":1}";
		mockMvc.perform(
				MockMvcRequestBuilders.put("/update/1").content(content).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1)).andDo(MockMvcResultHandlers.print())
				.andReturn();
	}

	@Test
	public void customerControllerException() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/exception/0").contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(MockMvcResultHandlers.print()).andReturn();
	}
	
	/**
	 * 模拟文件上传
	 * @throws Exception 
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void upDownControllerUpload() throws UnsupportedEncodingException, Exception {
		mockMvc.perform(
				// 上传URL路径
				fileUpload("/file/upload").file(
						new MockMultipartFile(
								"file", 	// form-data 名称
								"test.txt", // 上传的文件名
								"multipart/form-data",	// 上传类型
								"Hello upload".getBytes("UTF-8")	// 上传的文件的内容
								)
						)
				)
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print()).andReturn();
	}
}
