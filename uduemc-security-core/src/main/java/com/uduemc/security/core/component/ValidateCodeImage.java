package com.uduemc.security.core.component;

import javax.servlet.http.HttpServletRequest;

public interface ValidateCodeImage {
	
	public ImageCodeAndBufferedImage createImageCode(HttpServletRequest request);
}
