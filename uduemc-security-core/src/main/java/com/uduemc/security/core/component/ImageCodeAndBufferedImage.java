package com.uduemc.security.core.component;

import java.awt.image.BufferedImage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ImageCodeAndBufferedImage {
	/**
	 * 为了避免出现异常：java.io.NotSerializableException: java.awt.image.BufferedImage 将
	 * BufferedImage 和 ImageCode 分开
	 */
	private ImageCode imageCode;
	private BufferedImage bufferedImage;
}
