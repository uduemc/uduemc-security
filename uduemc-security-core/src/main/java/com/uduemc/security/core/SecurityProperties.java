package com.uduemc.security.core;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "uduemc.security")
public class SecurityProperties {
	private BrowserProperties browser = new BrowserProperties();
	private ValidateCodeProperties validate = new ValidateCodeProperties();
}
