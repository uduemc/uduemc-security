package com.uduemc.security.core;

import java.util.List;

import lombok.Data;

@Data
public class ValidateCodeProperties {

	private Integer width = 67;
	private Integer height = 23;
	private Integer length = 4;
	
	// 验证码的实现类名称
	private String achieveName = "defaultValidateCodeImage";
	// 需要验证码验证的url
	private List<String> accessUrl;
}
