package com.uduemc.security.core.exception;

import org.springframework.security.core.AuthenticationException;

public class ValidateCodeAuthenticationException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ValidateCodeAuthenticationException(String explanation) {
		super(explanation);
	}

}
