package com.uduemc.security.core;

import lombok.Data;

@Data
public class BrowserProperties {

	private String loginPage = "authentication-login";
}
