package com.uduemc.security.core.component;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ImageCode implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 存储的验证码
	private String code;
	// 过期时间
	private LocalDateTime expireTime;

	public ImageCode(String code, int expireTime) {
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds((long) expireTime);
	}
	
	/**
	 * 验证是否过期
	 * @return
	 */
	public boolean isExpire() {
		return LocalDateTime.now().isAfter(this.expireTime);
	}
}
